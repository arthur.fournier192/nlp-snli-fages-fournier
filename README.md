# Projet SNLI Sarah FAGES - Arthur FOURNIER

L'objectif de ce projet était la réalisation d'un classificateur multiclasse.
Il permet de déterminer si deux phrases :
- se suivent (entailment)
- forment une contradiction (contradiction)
- sont neutres (neutral)

Voici un exemple: <br>
Phrase 1: *I love F1* <br>
Phrase 2: *I hate F1* <br>
Ces deux phrases doivent alors être qualifiées de contradiction par notre modèle.



## Tableau comparatif des modèles entraînés
|                                   Model                                  | Loss | Accuracy |
|--------------------------------------------------------------------------|------|----------|
| [DistillBert](https://huggingface.co/distilbert/distilbert-base-uncased) | 0.30 | 88.93 %  |
| [Bert](https://huggingface.co/google-bert/bert-base-uncased)             | 0.27 | 90.56 %  |
| [Roberta](https://huggingface.co/FacebookAI/roberta-base)                | 0.25 | 91.45 %  |

## Conclusion

Après avoir comparé les performances des différents modèles, nous avons finalement décidé d'utiliser Roberta du fait de sa performance supérieure !
Nous avons aussi profité du temps restant pour réaliser une petite application afin de tester le modèle. Elle est disponible à l'adresse suivante : [nlp.fournierfamily.ovh](https://nlp.fournierfamily.ovh)

## Utilisation sur le modèle

Pour une expérimentation locale, commencez par configurer l'environnement :
```
python3 -m virtualenv venv
source venv/bin/activate
python -m pip -r requirements.txt
```

Ensuite, vous pouvez lancer un entrainement en modifiant le fichier YAML, puis:

```
python3.8 tp8.py train
```

Et pour tester:
```
python3.8 tp8.py test
```

## Utilisation de l'application streamlit
Pour une expérimentation locale, commencez par configurer l'environnement :
```
cd app
python3 -m virtualenv venv
source venv/bin/activate
python -m pip -r requirements.txt
```

puis:
```
export model_type="roberta"
export model_base="FacebookAI/roberta-base"
streamlit run app.py
```
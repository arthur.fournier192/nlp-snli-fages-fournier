
from torch.utils.data import DataLoader
from datasets import load_dataset

def preprocess_dataset(data, tokenizer, max_length):

    # we tokenize the premise and hypothesis, uding the padding to max_length, and we truncate sentences that are too long
    to_return = tokenizer(data["premise"], data["hypothesis"], padding="max_length", truncation=True, max_length=max_length)
    to_return["label"] = data["label"]
    return to_return

def get_train_val_dataloader(tokenizer, batch_size, max_length, num_worker, use_cuda):
    snli = load_dataset("snli")
    snli = snli.filter(lambda example: example['label'] != -1)

    # we apply the preprocessing function to the entire dataset and then we drop the original column premise and hypothesis
    snli["train"] = snli["train"].map(preprocess_dataset, fn_kwargs={"tokenizer": tokenizer, "max_length": max_length}, batched=True, num_proc=num_worker)
    snli["train"] = snli["train"].remove_columns("premise")
    snli["train"] = snli["train"].remove_columns("hypothesis")

    # we do the same for the validation set 
    snli["validation"] = snli["validation"].map(preprocess_dataset, fn_kwargs={"tokenizer": tokenizer, "max_length": max_length}, batched=True, num_proc=num_worker)
    snli["validation"] = snli["validation"].remove_columns("premise")
    snli["validation"] = snli["validation"].remove_columns("hypothesis")


    # we then create the Dataloader to do minibatch
    train_dataloader = DataLoader(snli["train"].with_format("torch"), batch_size=batch_size, num_workers=num_worker, pin_memory=use_cuda, shuffle=True)
    valid_dataloader = DataLoader(snli["validation"].with_format("torch"), batch_size=batch_size, num_workers=num_worker, pin_memory=use_cuda)

    return train_dataloader, valid_dataloader


def get_test_dataloader(tokenizer, batch_size, max_length, num_worker, use_cuda):
    snli = load_dataset("snli")
    snli = snli.filter(lambda example: example['label'] != -1)

    snli["test"] = snli["test"].map(preprocess_dataset, fn_kwargs={"tokenizer": tokenizer, "max_length": max_length}, batched=True, num_proc=num_worker)
    snli["test"] = snli["test"].remove_columns("premise")
    snli["test"] = snli["test"].remove_columns("hypothesis")
    # as we did with the training set and validation set we apply the tokenizer on the data and create a dataloader
    return DataLoader(snli["test"].with_format("torch"), batch_size=batch_size, num_workers=num_worker, pin_memory=use_cuda)
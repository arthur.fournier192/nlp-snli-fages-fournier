from transformers import AutoTokenizer, AutoModel
from transformers import logging as lg
import logging
import torch
from utils import train, test, ModelCheckpoint, generate_unique_logpath
from models import *
from data import *
import yaml
import wandb
from sys import argv
lg.set_verbosity_error()
import os


def training(config):
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    use_cuda = torch.cuda.is_available()
    scaler = torch.cuda.amp.GradScaler() # we create this object to be able to run at fp16 for training
    batch_size = config["data"]["batch_size"] # we get the different config from a yaml file
    num_worker = config["data"]["num_worker"]
    max_length = config["data"]["max_length"]
    base_model = config["model"]["base"]
    model_type = config["model"]["type"]
    EPOCH = config["train"]["epoch"]
    LR = config["optimizer"]["lr"]

    wandb.init(entity="space192",project="NLP",config=config) # we init the wandb run with the config logged

    tokenizer = AutoTokenizer.from_pretrained(base_model)
    bert = AutoModel.from_pretrained(base_model, output_attentions=True) # we get the Tokenizer and Model from huggingface 

    logging.info("= Building the dataloaders")

    train_dataloader, valid_dataloader = get_train_val_dataloader(tokenizer, batch_size, max_length, num_worker, use_cuda) # get the train dataloader
        
    logging.info("= Building the model")

    if model_type == "distill":
        model = BertNLIModelDistill(bert)
    else:
        model = BertNLIModel(bert) # initialization of the model with the bert model passed as parameter
    model.to(device)

    f_loss = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.AdamW(model.parameters(), lr=LR, eps=1e-6) # we set our optimzer with the parameters from the config file

    logdir = generate_unique_logpath("logs", model_type)

    if not os.path.isdir("logs"):
        os.makedirs("logs")

    if not os.path.isdir(logdir):
        os.makedirs(logdir)

    model_checkpoint = ModelCheckpoint(
        model, str(logdir + "/best_model.pt"), min_is_best=True
    )

    for i in range(EPOCH):
        train_loss, train_acc = train(train_dataloader, model, f_loss, optimizer, device, scaler,model_type)
        valid_loss, valid_acc = test(valid_dataloader, model, f_loss, device,model_type)

        model_checkpoint.update(valid_loss)

        wandb.log({"train_loss": train_loss, "train_acc": train_acc, "valid_loss": valid_loss, "valid_acc": valid_acc}) # we log the different metric to the wandb server

    sentence_1 = "Hi my name is Arthur"
    sentence_2 = "How are you ?"
    data = tokenizer(sentence_1, sentence_2, padding="max_length", truncation=True, max_length=max_length)


    dynamic_axes = {}
    if model_type == "bert":
        x = (torch.Tensor(data["input_ids"]).to(torch.int64).unsqueeze(0), torch.Tensor(data["attention_mask"]).to(torch.int64).unsqueeze(0),torch.Tensor(data["token_type_ids"]).to(torch.int64).unsqueeze(0))
        input_names = ["input_ids", "attention_mask","token_type_ids"]
        dynamic_axes["input_ids"] = {0: "batch"}
        dynamic_axes["token_type_ids"] = {0: "batch"}
        dynamic_axes["attention_mask"] = {0: "batch"}
    else:
        x = (torch.Tensor(data["input_ids"]).to(torch.int64).unsqueeze(0), torch.Tensor(data["attention_mask"]).to(torch.int64).unsqueeze(0))
        input_names = ["input_ids", "attention_mask"]
        dynamic_axes["input_ids"] = {0: "batch"}
        dynamic_axes["attention_mask"] = {0: "batch"}
    
    output_names = ["output"]
    dynamic_axes["output"] = {0: "batch"}
    model.load_state_dict(torch.load(logdir + "/best_model.pt", map_location=torch.device("cpu")))
    model.eval()
    model.to(torch.device("cpu"))
    torch.onnx.export(model, x, logdir + "/model.onnx",export_params=True, input_names=input_names, output_names=output_names, dynamic_axes=dynamic_axes)
    # by doing this we are converting the model to onnx, so get faster inference


    artifact = wandb.Artifact(name="model_onnx", type="model")
    artifact.add_file(local_path=str(logdir + "/model.onnx"))

    wandb.log_artifact(artifact)
    wandb.log_model(path=str(logdir + "/best_model.pt"), name="model")

    # we log the different artifact to wandb
    wandb.finish()

def testing(config):
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    use_cuda = torch.cuda.is_available()
    batch_size = config["data"]["batch_size"]
    num_worker = config["data"]["num_worker"]
    max_length = config["data"]["max_length"]
    base_model = config["model"]["base"]
    model_type = config["model"]["type"]
    model_to_load = config["test"]["model"]

    tokenizer = AutoTokenizer.from_pretrained(base_model)
    bert = AutoModel.from_pretrained(base_model, output_attentions=True)

    if model_type == "distill":
        model = BertNLIModelDistill(bert)
    else:
        model = BertNLIModel(bert)
    model.load_state_dict(torch.load(model_to_load))
    model.to(device)

    logging.info("= Building the dataloaders")

    test_dataloader = get_test_dataloader(tokenizer,batch_size,max_length,num_worker,use_cuda)

    f_loss = torch.nn.CrossEntropyLoss()

    test_loss, test_acc = test(test_dataloader, model, f_loss, device, model_type)

    print(f"test loss:{test_loss:.2f}")
    print(f"test acc:{(test_acc*100):.2f} %")



if __name__ == "__main__":
    config = yaml.safe_load(open("config.yaml", "r"))
    if argv[1] == "train":
        training(config)
    elif argv[1] == "test":
        testing(config)
    else:
        pass
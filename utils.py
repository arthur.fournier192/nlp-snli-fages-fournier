from tqdm import tqdm
import torch
import os

def generate_unique_logpath(logdir, raw_run_name):
    """
    Generate a unique directory name
    Argument:
        logdir: the prefix directory
        raw_run_name(str): the base name
    Returns:
        log_path: a non-existent path like logdir/raw_run_name_xxxx
                  where xxxx is an int
    """
    i = 0
    while True:
        run_name = raw_run_name + "_" + str(i)
        log_path = os.path.join(logdir, run_name)
        if not os.path.isdir(log_path):
            return log_path
        i = i + 1

class ModelCheckpoint(object):
    """
    Early stopping callback
    """

    def __init__(
        self,
        model: torch.nn.Module,
        savepath,
        min_is_best: bool = True,
    ) -> None:
        self.model = model
        self.savepath = savepath
        self.best_score = None
        if min_is_best:
            self.is_better = self.lower_is_better
        else:
            self.is_better = self.higher_is_better

    def lower_is_better(self, score):
        return self.best_score is None or score < self.best_score

    def higher_is_better(self, score):
        return self.best_score is None or score > self.best_score

    def update(self, score):
        if self.is_better(score):
            torch.save(self.model.state_dict(), self.savepath)
            self.best_score = score
            return True
        return False


def train(loader, model, criterion, optimizer, device, scaler, type):
    model.train()
    tot_loss = 0
    correct = 0
    N = 0
    t = tqdm(iter(loader),total=len(loader),leave=True)
    for e in t:

    # e = next(iter(loader))
    # for i in range(0,1000):
        optimizer.zero_grad(set_to_none=True)
        
        input_ids = e["input_ids"].to(device,non_blocking=True)
        if type == "bert":
            token_type_ids = e["token_type_ids"].to(device,non_blocking=True) # we copy all the data to the gpu
        attention_mask = e["attention_mask"].to(device,non_blocking=True)
        label = e["label"].to(device)
        with torch.cuda.amp.autocast(): # we then enable half precision
            if type == "bert":
                outputs, _ = model(input_ids=input_ids, attention_mask=attention_mask, token_type_ids=token_type_ids) # we go through the model
            else:
                outputs, _ = model(input_ids=input_ids, attention_mask=attention_mask) # we go through the model
            loss = criterion(outputs, label) # we calculate the loss

        tot_loss += input_ids.shape[0] * loss.item()
        predicted_targets = outputs.argmax(dim=1)
        correct += int((predicted_targets == label).sum().item())
        N += input_ids.shape[0]

        t.set_description(f"Train_CE : {tot_loss/N:.2f}, accuracy: {((correct / N)*100):.2f} %")
        t.refresh()
        # print(f"Train_CE : {tot_loss/N:.2f}, accuracy: {((correct / N)*100):.2f} %")
        scaler.scale(loss).backward() # then we scale it back to fp32 and the backward pass
        scaler.step(optimizer) # we optimize the NN
        scaler.update() # and we update our scaler
    return tot_loss/N,correct/N


def test(loader, model, criterion, device, type):
    with torch.no_grad(): # we start by disabling the gradiant all together
        model.eval() # we set our model to eval to disable dropout
        tot_loss = 0
        correct = 0
        N = 0
        t = tqdm(iter(loader),total=len(loader),leave=True)
        for e in t:
            
            input_ids = e["input_ids"].to(device,non_blocking=True)
            if type == "bert":
                token_type_ids = e["token_type_ids"].to(device,non_blocking=True)
            attention_mask = e["attention_mask"].to(device,non_blocking=True) #we copy the data to the gpu
            label = e["label"].to(device)
            with torch.cuda.amp.autocast(): # we enable fp16 casting to fit bigger models
                if type == "bert":
                    outputs,_ = model(input_ids=input_ids, attention_mask=attention_mask, token_type_ids=token_type_ids) # we do a forward pass on the model
                else:
                    outputs, _ = model(input_ids=input_ids, attention_mask=attention_mask)
                loss = criterion(outputs, label) # we apply our loss

            tot_loss += input_ids.shape[0] * loss.item()
            predicted_targets = outputs.argmax(dim=1)
            correct += int((predicted_targets == label).sum().item())
            N += input_ids.shape[0]
            t.set_description(f"Test_CE : {tot_loss/N:.2f}, accuracy: {((correct / N)*100):.2f} %")
            t.refresh()

        return tot_loss/N,correct/N
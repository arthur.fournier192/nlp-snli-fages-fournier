import streamlit as st
from utils import layout, image, link
from htbuilder.units import px
import matplotlib.pyplot as plt
import seaborn as sns
from transformers import AutoTokenizer, AutoModel
import numpy as np
import wandb
from os import environ
import re
from torch import nn
from torch.cuda import is_available
from torch import no_grad, load, device, Tensor, int32, softmax
from torch.backends.mps import is_available as mps_available

class BertNLIModel(nn.Module):
    def __init__(self, bert) -> None:
        super().__init__()

        self.bert = bert #we simply add a final Linear layer with 3 outputs on the end, and apply dropout to help training
        self.fc = nn.Linear(self.bert.config.hidden_size, 3)
        self.dp = nn.Dropout(0.2)

    def forward(self, input_ids, attention_mask, token_type_ids=None):
        if token_type_ids == None:
            x = self.bert(input_ids=input_ids, attention_mask=attention_mask)
        else:
            x = self.bert(input_ids=input_ids, token_type_ids=token_type_ids, attention_mask=attention_mask)
        #we pass the input through our bert model and recover attention, and we disable the gradiant on the attention
        detached_attentions = [att.detach() for att in x.attentions][0]
        
        x = self.dp(x[1])
        # we apply dropout on the pooler_output before passing it to the final layer
        x = self.fc(x)
        return x, detached_attentions

@st.cache_resource
def fetch_model():
    wandb.Api().artifact("space192/NLP/model:best").download(root=".")
    gpu = device("cuda" if is_available() else "mps" if mps_available() else "cpu")
    tokenizer = AutoTokenizer.from_pretrained(environ["model_base"])
    bert = AutoModel.from_pretrained(environ["model_base"], output_attentions=True)
    model = BertNLIModel(bert)
    model.load_state_dict(load("best_model.pt",map_location=gpu))
    model.to(gpu)
    model.eval()
    return tokenizer, model, gpu
tokenizer, model, gpu = fetch_model()

label = ["entailment","neutral", "contradiction"]

st.markdown("""
    <style>
        .reportview-container {
            margin-top: -2em;
        }
        #MainMenu {visibility: hidden;}
        .stDeployButton {display:none;}
        footer {visibility: hidden;}
        #stDecoration {display:none;}
    </style>
""", unsafe_allow_html=True)

st.title('SDI-Metz SNLI')

sentence1 = st.text_input(label='first sentence', placeholder='first sentence', label_visibility='hidden')
sentence2 = st.text_input(label='second sentence', placeholder='second sentence', label_visibility='hidden')

if st.button('Run Model'):
    data = tokenizer(sentence1, sentence2, padding="max_length", truncation=True, max_length=128)

    with no_grad():
        if environ["model_type"] == "bert":
            x = (Tensor(data["input_ids"]).to(int32).unsqueeze(0).to(gpu), Tensor(data["attention_mask"]).to(int32).unsqueeze(0).to(gpu),Tensor(data["token_type_ids"]).to(int32).unsqueeze(0).to(gpu))
            r,attention = model(input_ids=x[0],attention_mask=x[1], token_type_ids=x[2])
        else:
            x = (Tensor(data["input_ids"]).to(int32).unsqueeze(0).to(gpu), Tensor(data["attention_mask"]).to(int32).unsqueeze(0).to(gpu))
            r,attention = model(input_ids=x[0],attention_mask=x[1])
    
        r = r.cpu()
        attention = attention.cpu()

    biggest = np.argmax(r[0].numpy())
    soft_out = softmax(r, dim=1)

    attention_weights = attention[0][0].numpy()

    if environ["model_type"] == "bert":
        size = len(sentence1.split()) + len(sentence2.split()) + 3
        result = tokenizer.decode(data["input_ids"]).split()[0:size]
    else:
        string = tokenizer.decode(data["input_ids"])
        pattern = re.compile(r'(<\/?s>|<pad>|[\w\d]+)')
        result = pattern.findall(string)
        result = [s.strip() for s in result if s.strip() != '' and s.strip() != "<pad>"]
        size = len(result)


    fig = plt.Figure(figsize=(10, 8))
    ax = fig.subplots()
    
    sns.heatmap(attention_weights[0:size, 0:size], cmap='viridis', annot=True, ax=ax,yticklabels=False)
    ax.set_yticks([x + 0.5 for x in range(0, size)])
    ax.set_yticklabels(result, rotation=0)
    ax.set_xlabel('To')
    ax.set_ylabel('From')
    st.write(f"prediction: {label[biggest]} with {(soft_out[0].numpy()[biggest]*100):.2f} % of confidence")
    st.pyplot(fig)

myargs = [
        "Made with ",
        image("https://streamlit.io/images/brand/streamlit-mark-color.png",
              width=px(25), height=px(25)),
        " by ",
         link("https://github.com/sarahfages", "Sarah Fages"),
        " and ",
        link("https://github.com/space192", "Arthur Fournier")
    ]
layout(*myargs)

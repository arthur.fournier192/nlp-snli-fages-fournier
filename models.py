from torch import nn

class BertNLIModel(nn.Module):
    def __init__(self, bert) -> None:
        super().__init__()

        self.bert = bert #we simply add a final Linear layer with 3 outputs on the end, and apply dropout to help training
        self.fc = nn.Linear(self.bert.config.hidden_size, 3)
        self.dp = nn.Dropout(0.2)

    def forward(self, input_ids, attention_mask, token_type_ids=None):
        if token_type_ids == None:
            x = self.bert(input_ids=input_ids, attention_mask=attention_mask)
        else:
            x = self.bert(input_ids=input_ids, token_type_ids=token_type_ids, attention_mask=attention_mask)
        #we pass the input through our bert model and recover attention, and we disable the gradiant on the attention
        detached_attentions = [att.detach() for att in x.attentions][0]
        
        x = self.dp(x[1])
        # we apply dropout on the pooler_output before passing it to the final layer
        x = self.fc(x)
        return x, detached_attentions
    

class BertNLIModelDistill(nn.Module):
    def __init__(self, bert) -> None:
        super().__init__()

        self.bert = bert #we simply add a final Linear layer with 3 outputs on the end, and apply dropout to help training
        self.fc = nn.Linear(self.bert.config.hidden_size, 3)
        self.dp = nn.Dropout(0.2)

    def forward(self, input_ids, attention_mask):
        x = self.bert(input_ids=input_ids, attention_mask=attention_mask)
        #we pass the input through our bert model and recover attention, and we disable the gradiant on the attention
        detached_attentions = [att.detach() for att in x.attentions][0]
        
        x = x.last_hidden_state[:, 0]

        x = self.dp(x)
        # we apply dropout on the pooler_output before passing it to the final layer
        x = self.fc(x)
        return x, detached_attentions